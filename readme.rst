﻿* Autor: Jon Gimenez Sanchez
=======

* Fecha: 14/02/2014
=======

**Poryecto**
============

En esta tarea de un sitio web de 10 paginas, he elegido como tema principal "Red Bull", dada la versatilidad que ofrece a la hora de realizar el numero de páginas requeridas. Consta de una pagina principal, nueve páginas explicando nueve de las muchas actividades que patrocina la marca y una ultima pagina de contacto.

**Conclusion**
==============

Me ha parecido un poco complicado realizar el proyecto debido al uso exclusiva del bloc de notas y no poder usar un editor como el dreamweber, para maquetar y "programar" el sitio web. El buscar el código adecuado para un resultado medianamente atractivo es costoso y laborioso, mas y cuando en clase el código visto ha sido bastante limitado.